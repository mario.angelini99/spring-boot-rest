package com.bhaiti.kela.orm.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import com.bhaiti.kela.entity.Studenti;

public class StudentiJPA {

	public static void main(String[] args) {

		EntityManagerFactory entityManagerFactory = Persistence
				.createEntityManagerFactory("custom-persistence-context");
		EntityManager entityManager = entityManagerFactory.createEntityManager();

		// Insert
		entityManager.getTransaction().begin();
		Studenti studente = new Studenti();
		studente.setMatricola("5678");
		studente.setNome("Lucas");
		studente.setCognome("Rossi");
		studente.setAge(21);
		entityManager.persist(studente);
		entityManager.getTransaction().commit();

		// Read
		Query read = entityManager.createQuery("select t from Studenti t");
		List<Studenti> studList = read.getResultList();
		for (Studenti stud : studList) {
			System.out.println(stud);
		}

		// update
		entityManager.getTransaction();
		entityManager.getTransaction().begin();
		Studenti studentiFromDB = entityManager.find(Studenti.class, "5678");
		studentiFromDB.setAge(32);
		entityManager.getTransaction().commit();
		System.out.println(studentiFromDB);

		// delete
		entityManager.getTransaction();
		entityManager.getTransaction().begin();
		entityManager.remove(studentiFromDB);
		entityManager.getTransaction().commit();

		entityManager.close();
	}

}
