package com.bhaiti.kela.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;

@Entity
@NamedQuery(name = "Studenti.findAll", query = "SELECT s FROM Studenti s")
public class Studenti implements Serializable {
	private static final long serialVersionUID = 1L;

	private int age;

	private String cognome;

	@Id
	private String matricola;

	private String nome;

	public Studenti() {
	}

	public int getAge() {
		return this.age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getCognome() {
		return this.cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public String getMatricola() {
		return this.matricola;
	}

	public void setMatricola(String matricola) {
		this.matricola = matricola;
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public String toString() {
		return "Studente{" + "matricola=" + matricola + ", nome='" + nome + '\'' + ", cognome='" + cognome + '\''
				+ ", età='" + age + '\'' + '}';
	}

}