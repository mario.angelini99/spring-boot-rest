package com.bhaiti.kela.dao;

import java.util.List;

import com.bhaiti.kela.entity.Studenti;

/***
 * In questa interfaccia sono definiti tutti i metodi utili per effettuare
 * operazioni CRUD con la classe Studenti
 * 
 * @author Mario Angelini
 *
 */
public interface StudentiDAO {
	/**
	 * Create
	 */
	public boolean insertStudente(Studenti studente);

	/**
	 * Read
	 */
	public List<Studenti> getStudenti();

	/**
	 * Update
	 */
	public boolean updateStudente(Studenti studente);

	/**
	 * Delete
	 */
	public boolean deleteStudente(Studenti studente);

}
